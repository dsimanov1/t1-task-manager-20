package ru.t1.simanov.tm.command.task;

import ru.t1.simanov.tm.api.service.IProjectTaskService;
import ru.t1.simanov.tm.api.service.ITaskService;
import ru.t1.simanov.tm.command.AbstractCommand;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    public void renderTasks(final List<Task> tasks) {
        for (final Task task : tasks) {
            showTask(task);
            System.out.println();
        }
    }

    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + task.getCreated());
    }


}

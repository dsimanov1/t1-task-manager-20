package ru.t1.simanov.tm.api.repository;

import ru.t1.simanov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator comparator);

    M add(final String userId, M model);

    boolean existsById(String userId, String id);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M remove(final String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

}

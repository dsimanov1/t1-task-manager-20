package ru.t1.simanov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
